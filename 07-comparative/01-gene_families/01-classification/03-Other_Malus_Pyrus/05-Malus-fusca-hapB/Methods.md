Download the protein sequences from GDR and unzip the .gz file to a fasta file.
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_fusca/Malus_fusca_v1/genes/Mfusca_v1.0_hap2.proteins.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_fusca/Malus_fusca_v1/genes/Mfusca_v1.0_hap2.CDS.fa.gz
gunzip Mfusca_v1.0_hap2.proteins.fa.gz
gunzip Mfusca_v1.0_hap2.CDS.fa.gz
```

Run PlantTribes2 geneFamilyClassifier with the 26Gv2.0 database
```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```
