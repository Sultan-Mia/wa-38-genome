Download the protein sequences from GDR and unzip the .gz file to a fasta file.
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_prunifolia/crabapple_Fupingqiuzi_v1/genes/Malus_prunifolia_Fupingqiuzi.proteins.fasta.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_prunifolia/crabapple_Fupingqiuzi_v1/genes/Malus_prunifolia_Fupingqiuzi.CDS.fasta.gz
gunzip Malus_prunifolia_Fupingqiuzi.proteins.fasta.gz
gunzip Malus_prunifolia_Fupingqiuzi.CDS.fasta.gz
```

Run PlantTribes2 geneFamilyClassifier with the 26Gv2.0 database
```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```
