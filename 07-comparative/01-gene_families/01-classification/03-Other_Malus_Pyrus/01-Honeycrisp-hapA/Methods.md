Download the protein sequences from GDR and unzip the .gz file to a fasta file.
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.cds.fa.gz
gunzip g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
gunzip g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.cds.fa.gz
```

Run PlantTribes2 geneFamilyClassifier with the 26Gv2.0 database
```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```
