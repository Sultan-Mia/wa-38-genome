Directory Overview
==================
This directory contains steps of running comparative genomics.  
Authors: Huting Zhang  
Last updated: Dec 14th 2023  

Directory Structure
===================
The following describes the purpose for each directory:
### 01-gene_families  
This directory contains gene family analysis steps, including gene family classification using [PlantTribes2](https://github.com/dePamphilis/PlantTribes) and [CROG analysis](https://www.frontiersin.org/articles/10.3389/fpls.2022.1011199/full).
- 01-classification  
This directory contain gene family classification of multiple apple and pear genome annotations
    * 01-hapA  
    Gene family classification of WA 38 HapA annotation  
    * 02-hapB  
    Gene family classification of WA 38 HapB annotation  
    * 03-Other_Malus_Pyrus  
    Gene family classification of other recently published Malus and Pyrus genome annotations  
        - 01-Honeycrisp-hapA  
        - 02-Honeycrisp-hapB  
        - 03-Malus-baccata   
        - 04-Malus-fusca-hapA  
        - 05-Malus-fusca-hapB  
        - 06-Malus-prunifolia  
        - 07-dAnjou-hapA  
        - 08-dAnjou-hapB    
        - 09-Gala  
        - 10-Malus-sieversii  
        - 11-Malus-sylvestris  
        - 12-HanFu  
        - 13-GDDH13  

- 02-analysis  
This directory contains analysis of shared and unique orthogroup among M. domestica genomes and CoRe OrthoGroup (CROG) analysis.  

### 02-synteny
