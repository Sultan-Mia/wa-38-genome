Before we can run PASA to identify UTRs, we need to createa reference-guided 
transcriptome assembly.  To do this, we need to combine the BAM files from the 
RNA-seq alignments into a single large BAM file.  Because we will want to load
thse BAM files into viewers (such as IGV) we will also create combined BAM files
with only 5% and 1% of RNA-seq reads. However, we will use the entire large BAM
file for the reference guided assembly.

```bash
sbatch 01-BAM_merge-hapA.srun
sbatch 02-BAM_index-hapA.srun
sbatch 03-BAM_sample-5perc-hapA.srun
sbatch 04-BAM_sample_1perc-hapA.srun
```

Now run Trinity:

```bash
sbatch 01-Trinity.srun
```
