#!/bin/bash

# Start the MySQL Server
export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
mysqld_safe --datadir=$PWD/mysql-files --log-error=$PWD/mysql-files/error.log &

# Wait to get going until MySQL is fully running
while ! mysqladmin ping -h "localhost" -u root --silent; do
  echo "Waiting on MySQL..."
  sleep 10
done

# Setup the PASA config file so PASA knows how to talk to mysql. 
export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
socket_path=$(echo $MYSQL_UNIX_PORT | perl -p -e 's/\//\\\//g')
cp pasa.config.template pasa.config
perl -pi -e "s/__SOCKET__/$socket_path/" pasa.config
export PASACONF=$PWD/pasa.config

$PASAHOME/scripts/build_comprehensive_transcriptome.dbi \
    -c alignAssembly.config \
    -t Trinity-combined.fasta \
    --min_per_ID 95 \
    --min_per_aligned 30

# Cleanly shutdown MySQL
mysql -u root -e  "SHUTDOWN;"

sleep 30
