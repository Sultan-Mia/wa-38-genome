#!/bin/bash

# Start the MySQL Server
export MYSQL_UNIX_PORT=/tmp/mysqld.sock
mysqld_safe --datadir=$PWD/mysql-files --log-error=$PWD/mysql-files/error.log &

# Wait to get going until MySQL is fully running
while ! mysqladmin ping -h "localhost" -u root --silent; do
  echo "Waiting on MySQL..."
  sleep 10
done

# Setup the PASA config file so PASA knows how to talk to mysql. 
export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
socket_path=$(echo $MYSQL_UNIX_PORT | perl -p -e 's/\//\\\//g')
cp pasa.config.template pasa.config
perl -pi -e "s/__SOCKET__/$socket_path/" pasa.config
export PASACONF=$PWD/pasa.config

# Setup the PasaWeb config file and rn the lighttpd daemon. We can't use
# the run_PasaWeb.pl to do this for us because it wants to edit directories
# in the docker image and that's not allowed with singularty.
cwd=`pwd`
cwd_path=$(echo $cwd | perl -p -e 's/\//\\\//g')
pasa_path=$(echo $PASAHOME | perl -p -e 's/\//\\\//g')
perl_path=$(which perl | perl -p -e 's/\//\\\//g')
perl5lib=$(perl -e "print join(':', @INC)" | perl -p -e 's/\//\\\//g')

cp /usr/local/src/PASApipeline/PasaWeb.conf/lighttpd.conf.template ./lighthttpd.conf
perl -pi -e "s/__DOCUMENT_ROOT__/$pasa_path\/PasaWeb/" lighthttpd.conf
perl -pi -e "s/__PORT_NO__/8080/" lighthttpd.conf
perl -pi -e "s/__PERL_PATH__/$perl_path/" lighthttpd.conf
perl -pi -e "s/__PERL5LIB__/\"PERL5LIB\" => \"$perl5lib:$pasa_path\/PerlLib:$pasa_path\/PasaWeb\/cgi-bin\"/" lighthttpd.conf
perl -pi -e "s/#server.errorlog\s+=\s+\".*\"/server.errorlog=\"$cwd_path\/lighthttpd.error.log\"/" lighthttpd.conf
perl -pi -e "s/#accesslog.filename\s+=\s+\".*\"/accesslog.filename=\"$cwd_path\/lighthttpd.access.log\"/" lighthttpd.conf

# Start the daemon. It will shut itself down automatically after 10 minutes
# of no activity.
lighttpd -D -i 600 -f $cwd/lighthttpd.conf

# Cleanly shutdown MySQL
mysql -u root -e  "SHUTDOWN;"
