Here we want to get some basic statistics about gene features in hapA and hapB. We have ~6200 more genes in hapB than hapA, which is totally unexpected and we want to know why. First of all, we want to 1) calculate the average and total protein length of all the proteins; 2) calculate the average and total protein length on each chromosome; 3) gene count in each chromosome; and compare the results between the 2 haps.  

Create sym links to the CDS and protein files from the annotation
```bash
ln -s ../../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
ln -s ../../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa
```

### Count of protein length
First, we count the length of each protein using bioawk. Bioawk is already installed in 01-input_data.
```bash
../../../../01-input_data/bioawk/bioawk -c fastx '{ print $name, length($seq)}' Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa > hapA.protein.length.txt
../../../../01-input_data/bioawk/bioawk -c fastx '{ print $name, length($seq)}' Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa > hapB.protein.length.txt
```
Next, we can calculate the average protein length and total length using:
```bash
awk '{x+=$2}END{print x/NR}' hapA.protein.length.txt
awk '{x+=$2}END{print x}' hapA.protein.length.txt
awk '{x+=$2}END{print x/NR}' hapB.protein.length.txt
awk '{x+=$2}END{print x}' hapB.protein.length.txt
```
Which gave us an average protein length of 376.13 for hapA and 355.094 for hapB. A total amino acid count of 23942542 in hapA and 25000042 in hapB.  

How about the primary transcript protein length (Note! t1 is not always the longest transcript)?
```bash
grep "t1" hapA.protein.length.txt > hapA.protein.t1.length.txt
awk '{x+=$2}END{print x/NR}' hapA.protein.t1.length.txt
#365.253
awk '{x+=$2}END{print x}' hapA.protein.t1.length.txt
#17622010
grep "t1" hapB.protein.length.txt > hapB.protein.t1.length.txt
awk '{x+=$2}END{print x/NR}' hapB.protein.t1.length.txt
#343.007
awk '{x+=$2}END{print x}' hapB.protein.t1.length.txt
#18693208
```
Now let's seperate each chromosome. First generate a list of all the chromosome IDs. I did that with `nano`.
```bash
for i in $(cat hapA.chr.IDs); do grep "$i" hapA.protein.length.txt > hapA.$i.protein.length.txt; done
for i in $(cat hapB.chr.IDs); do grep "$i" hapB.protein.length.txt > hapB.$i.protein.length.txt; done
```
Next, get the average and total protein length of each chromosome
```bash
for i in $(cat hapA.chr.IDs); do awk '{x+=$2}END{print x/NR}' hapA.$i.protein.length.txt; done
for i in $(cat hapA.chr.IDs); do awk '{x+=$2}END{print x}' hapA.$i.protein.length.txt; done
for i in $(cat hapB.chr.IDs); do awk '{x+=$2}END{print x/NR}' hapB.$i.protein.length.txt; done
for i in $(cat hapB.chr.IDs); do awk '{x+=$2}END{print x}' hapB.$i.protein.length.txt; done
```
Save the outputs in a table.  

Next, get the length of proteins translated from the t1 (primary transcripts) for each chromosome.
```bash
for i in $(cat hapA.chr.IDs); do grep "t1" hapA.$i.protein.length.txt > hapA.$i.protein.t1.length.txt; done
for i in $(cat hapB.chr.IDs); do grep "t1" hapB.$i.protein.length.txt > hapB.$i.protein.t1.length.txt; done
```
Count the average and total length of primary transcript protein length for each chromosome
```bash
for i in $(cat hapA.chr.IDs); do awk '{x+=$2}END{print x/NR}' hapA.$i.protein.t1.length.txt; done
for i in $(cat hapA.chr.IDs); do awk '{x+=$2}END{print x}' hapA.$i.protein.t1.length.txt; done
for i in $(cat hapB.chr.IDs); do awk '{x+=$2}END{print x/NR}' hapB.$i.protein.t1.length.txt; done
for i in $(cat hapB.chr.IDs); do awk '{x+=$2}END{print x}' hapB.$i.protein.t1.length.txt; done
```
Last, count how many genes are annotated from each chromosome
```bash
for i in $(cat hapB.chr.IDs); do wc -l hapB.$i.protein.t1.length.txt; done
for i in $(cat hapA.chr.IDs); do wc -l hapA.$i.protein.t1.length.txt; done
```

All the results are summarized in a table named `protein_number_length_summary`.

### transcript and splice variant count
First, files with gene ID and cooresponding counts of transcripts are created
```bash
grep ">" Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa | sed 's/>//' | awk '{print $1}' | sed 's/.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' > hapA.transcript.count.txt
grep ">" Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa | sed 's/>//' | awk '{print $1}' | sed 's/.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' > hapB.transcript.count.txt
```

Now calculate the average number of splice vatiants for each gene
```bash
awk '{total += $2; count++} END {print total/count}' hapA.transcript.count.txt 
#1.3199
awk '{total += $2; count++} END {print total/count}' hapB.transcript.count.txt 
#1.29231
```

Next, count how many genes have splice variants
```bash
cut -f2 hapA.transcript.count.txt | uniq -c
#      1  13
#      2  11
#     11  10
#     16  9
#     37  8
#     53  7
#    124  6
#    330  5
#    731  4
#   1773  3
#   6913  2
#  38236  1
cut -f2 hapB.transcript.count.txt | uniq -c
#      1  12
#      3  11
#     10  10
#     12  9
#     45  8
#     57  7
#    124  6
#    286  5
#    742  4
#   1854  3
#   7343  2
#  44002  1
```
The number of genes contain more than one splice variants in hapA is: 1+2+11+16+37+53+124+330+731+1773+6913=9991  
The number of genes contain more than one splice variants in hapA is: 1+3+10+12+45+57+124+286+742+1854+7343=10477  

### UTR counts
Create sym links to annotation GFF files:
```bash
ln -s ../../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 
ln -s ../../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3
```

Now count the total number of mRNA and the total number of mRNAs with UTR
```bash
cut -f3 Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 | grep -c "mRNA"
#63655
cat Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 | cut -f9 | grep "prime_UTR"| sed 's/ID=//' | sed 's/;.*//' | sed 's/.five.*//' |  sed 's/.three.*//' | sort | uniq | wc -l
#36424
cut -f3 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 | grep -c "mRNA"
#70404
cat Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 | cut -f9 | grep "prime_UTR"| sed 's/ID=//' | sed 's/;.*//' | sed 's/.five.*//' |  sed 's/.three.*//' | sort | uniq | wc -l
#36709
```
