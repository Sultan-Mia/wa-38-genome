The Python script named `generate_structural_files.py` was written sepecifically for this
project and will take the results from TSEBRA and RepeatMasker and generate GFF and FASTA 
files with features (e.g. genes) properly named following the naming convenstions, 
similar to the following:  Maldo.cc.v1a1.chr15A.g000010

With components of the name being:

- Maldo: 5-letter abbreviation for teh genus and species
- cc: 2-letter abbreviation for the cultivar
- v1: The version for the assembly
- a1: The version for the annotation
- chr15A: The chromosome on which the feature is found
- g000010: The gene locus ID.  Genes are numbered in increments of 10.

Transcripts will be given an additional prefix of ".t1" where the number 
increases by incremeents of 1 for each splice variant.

You get get help instructions for running the script by running it with the `-h` flag:
```bash
./generate_structural_files.py -h
```

Create sym links to all the assembly files
```bash
ln -s ../../../04-nuclear_assembly/11-filtering/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../../../04-nuclear_assembly/11-filtering/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
```

Create sym links to the repeat predictions
```bash
ln -s ../../01-annotate_repeats/02-RepeatMasker/01-hapA/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.out Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.repeats.out
ln -s ../../01-annotate_repeats/02-RepeatMasker/02-hapB/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.out Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.repeats.out
```

Create sym links to the gene predictions
```bash
ln -s ../07-FilterGenes/WA_38.gene_structures_post_PASA_updates.hapA.filtered.gff3
ln -s ../07-FilterGenes/WA_38.gene_structures_post_PASA_updates.hapB.filtered.gff3
```

Now run the scripts for generating the files
```bash
sbatch 01-files_hapA.srun
sbatch 02-files_hapB.srun
```

Once the files are generated, we run BUSCO to estimate the completeness of our annotation
```bash
sbatch 03-BUSCO_hapA.srun
sbatch 04-BUSCO_hapB.srun
```
