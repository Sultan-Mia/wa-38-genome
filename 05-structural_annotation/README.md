Directory Overview
==================
This directory contains steps of performing structural annotation, including repeat annotation and gene space annotation.  
Authors: Huiting Zhang  
Last Updated: Dec 8th 2023  

Directory Structure
===================
The following describes the purpose for each directory:

### 01-annotate_repeats
Two spteps were used for repeat annotation. EDTA was used to identify repetitive elements and RepeatMasker was used to annotate additional low complexity elements and mask the genome.  
- 01-EDTA
- 02-RepeatMasker

### 02-annotate-genes
BRAKER and TSEBRA was used to perform ab initio and evidence-based gene annotation. PASA was then deployed to polish the gene models and add UTR annotations. A custom script was then used to filter erroneous gene models.  
- 01-GEMmaker  
GEMmaker was used to align RNA-seq reads to the assemblies
- 02-Braker1_RNAseq  
This step performs *ab initio* prediction and evidence-based annotation using transcriptome data  
- 03-Braker2_protein  
This step performs *ab initio* prediction and evidence-based annotation using homologous proteins  
- 04-TSEBRA  
Transcript selector to merge gene models predicted from the 2 predictions  
- 05-Trinity  
TRINITY *de novo* and reference-guided transcriptome assemblies, which are used as inputs for PASA  
- 06-PASA  
Gene model polishing and UTR annotation  
- 07-FilterGenes  
Erronous gene models, e.g. removal of overlapping gene models, splitting non-overalpping splice variants into seperate gene models, etc, using a custom script  
- 08-Files  
A custom script was used to generate the sequence fasta files. Genes and repeats were renamed following a pre-established naming convention.  
- 09-annotation_stats  
Basic statisitics (e.g. protein length, number of splice variants per gene, etc) were calculated.  
