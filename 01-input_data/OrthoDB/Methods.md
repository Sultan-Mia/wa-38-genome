## OrthoDB Rosaceae Proteins

Download the level2species.tab file and the all_fasta.tab files from OrthoDB:

```bash
wget https://data.orthodb.org/download/odb11v0_all_fasta.tab.gz
wget https://data.orthodb.org/download/odb11v0_level2species.tab.gz

gunzip odb11v0_all_fasta.tab.gz
gunzip odb11v0_level2species.tab.gz
```

Create the list of OrthoDB species IDs using the NCBI Taxonomy IDs
that have a Rosaceae lineage.  We will format this list with a bar so 
it can be used in an egrep of the FASTA file.
```bash
match=`grep "2759,33090,3193,71240,3744,3745" odb11v0_level2species.tab | \
  awk '{print ">"$2":"}' | \
  perl -p -e 's/\n/|/g' | \
  perl -p -e 's/\|$//g'`
```

Change the FASTA formatting so that each sequences is on its own line, then
use `egrep` to find all of the matches and recreatea new FASTA file
```bash
cat odb11v0_all_fasta.tab | \
  perl -p -e 's/^(>.*)\n/\1\t/g' | \
  egrep $match | \
  awk -F"\t" '{print $1"\t"$2"\n"$3}' > OrthoDB_Rosaceae.faa
```

Now do the same for Virdiplantae.
```bash
match=`grep "2759,33090" odb11v0_level2species.tab | \
  awk '{print ">"$2":"}' | \
  perl -p -e 's/\n/|/g' | \
  perl -p -e 's/\|$//g'`

cat odb11v0_all_fasta.tab | \
  perl -p -e 's/^(>.*)\n/\1\t/g' | \
  egrep $match | \
  awk -F"\t" '{print $1"\t"$2"\n"$3}' > OrthoDB_Virdiplantae.faa
```
