Need how the input file is created.  

MitoHifi requires a reference mitochndria assembly and annotation. The Malus domestica mitochondria sequencing from NCBI (NC_018554.1) wad downloaded in both FASTA format and GenBank format, and saved as `Malusxdomestica_mito.fasta` and `Malusxdomestica_mito.gb`, respectively.  

Run MitoHifi
```bash
sbatch 01-mito_hifi_contigs.srun
```