### Run EnTAP

Create sym link to the hapB protein files
```bash
ln -s ../../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
```

Interpro scan can not take sequences with '*', so remove all '*' from the file first
```bash
cat Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa | sed 's/*//' > Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.faa
```

Run EnTap
```bash
sbatch 02_entap_hapB.srun
```

### Results intepretation
First, make a new directory in which we will analyze the output from EnTAP
```bash
mkdir 01-interpretation
```
