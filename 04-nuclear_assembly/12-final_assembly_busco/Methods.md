Before we run BUSCO we need access to the final scaffold FASTA file. 
We will create sym links to files we saved after Juicebox curation. BUSCO
can process each of our haplotypes at the same time if we put them in 
the same directory, so the following code creates a new directory
named `busco_input_files` and creates sym links there.

```bash
mkdir busco_input_files
cd busco_input_files
ln -s ../../11-filtering/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../../11-filtering/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
cd ..

```

Now run BUSCO on these FASTA files

```bash
sbatch 01-busco.srun
```
