Input_file	Dataset	Complete	Single	Duplicated	Fragmented	Missing	n_markers	Scaffold N50	Contigs N50	Percent gaps	Number of scaffolds
WA_38-hap1-JBAT.FINAL.fa	eudicots_odb10	98.7	58.8	39.9	0.5	0.8	2326	36115806	34751520	0.000%	699
WA_38-hap2-JBAT.FINAL.fa	eudicots_odb10	98.7	59.7	39.0	0.5	0.8	2326	37154807	35835625	0.000%	229
