# Use MUMmer to align 'WA 38' haplome assemblies  

Create sym links to the final assemblies
```bash
ln -s ../11-filtering/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../11-filtering/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
```

Run MUMmer alignment
```bash
sbatch 01-mummer.srun
```

Compress the delta files and uploadeto [Assemblytics](http://assemblytics.com/) to generate summary files.  
```bash
gzip WA_38_final_haps.delta
```

The result from Assemblitics is here:
--http://assemblytics.com/analysis.php?code=MFV33JIDk6OlKIKKKRM6  
