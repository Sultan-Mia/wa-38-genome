#!/bin/bash

kraken2-build --download-taxonomy --db contaminants --threads 20
kraken2-build --download-library bacteria --db contaminants --threads 20
kraken2-build --download-library viral --db contaminants --threads 20
kraken2-build --build --db contaminants --threads 20
