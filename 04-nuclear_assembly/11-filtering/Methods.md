## Get Scaffolds
For this step we need to separate the chromosomal pseudomolecules from the remaining 
scaffolds, as these could contain contaminants and plastid sequences.  The 
seqkit tool will be used to extract the scaffold sequences into separate FASTA files 
for filtering.

First, link to the assembly FASTA files
```bash
ln -s ../10-reorient/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa  Malus-domestica-WA_38_hapA-genome-v1.0.a1.all.fa
ln -s ../10-reorient/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa Malus-domestica-WA_38_hapB-genome-v1.0.a1.all.fa
```

Next, download and uncompress the seqkit tool.
```bash
wget https://github.com/shenwei356/seqkit/releases/download/v2.4.0/seqkit_linux_amd64.tar.gz
tar -zxvf seqkit_linux_amd64.tar.gz 
```

Next, get the names of the scaffolds and save them into separate files so we use them with seqkit.
```bash
grep ">scaffold" Malus-domestica-WA_38_hapA-genome-v1.0.a1.all.fa | sed -e 's/>//g' > scaffolds-hapA.txt
grep ">scaffold" Malus-domestica-WA_38_hapB-genome-v1.0.a1.all.fa | sed -e 's/>//g' > scaffolds-hapB.txt
grep ">chr" Malus-domestica-WA_38_hapA-genome-v1.0.a1.all.fa | sed -e 's/>//g' > chroms-hapA.txt
grep ">chr" Malus-domestica-WA_38_hapB-genome-v1.0.a1.all.fa | sed -e 's/>//g' > chroms-hapB.txt
```

Use seqkit to create new FASTA files of just the scaffolds and the known chromoomes
```bash
./seqkit grep -n -f scaffolds-hapA.txt Malus-domestica-WA_38_hapA-genome-v1.0.a1.all.fa > Malus-domestica-WA_38_hapA-scaffolds-v1.0.a1.fa
./seqkit grep -n -f scaffolds-hapB.txt Malus-domestica-WA_38_hapB-genome-v1.0.a1.all.fa > Malus-domestica-WA_38_hapB-scaffolds-v1.0.a1.fa
./seqkit grep -n -f chroms-hapA.txt Malus-domestica-WA_38_hapA-genome-v1.0.a1.all.fa > Malus-domestica-WA_38_hapA-chroms-v1.0.a1.fa
./seqkit grep -n -f chroms-hapB.txt Malus-domestica-WA_38_hapB-genome-v1.0.a1.all.fa > Malus-domestica-WA_38_hapB-chroms-v1.0.a1.fa
```
## Filter Plastids

The first step in filtering is to identify the plastid sequences
and separate them. We will do this by perfoming a megablast against
the RefSeq Plastid database.
```bash
01-download_plastid.srun
02-index_plastid.srun
03-blast_plastid_hapA.srun
04-blast_plastid_hapB.srun
```

Filtering results is quick with a custom Python script:

On Kamiak:
```bash
module add python3
python3 filter_plastids.py --haplotype hapA
python3 filter_plastids.py --haplotype hapB
```

## Filter contaminants
First build a custom Kraken2 database containing virus and bacterial sequence
from NCBI RefSeq. 
```bash
07-kraken-db.srun
```

Second, run Kraken2 to classify each scaffold (if there is proper alignment) as a contaminant. 
Kraken2 will generate a new FASTA file of contaminants.
```bash
srun 08-kraken2_hapA.srun
srun 09-kraken2_hapB.srun
```

## Create new genome file
It turns out that all scaffolds were either plastid or bacterial contaminants so, we can
create a new genome file that contains only the chromosomal sequences:
```bash
cat Malus-domestica-WA_38_hapA-chroms-v1.0.a1.fa > Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
cat Malus-domestica-WA_38_hapB-chroms-v1.0.a1.fa > Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
```

