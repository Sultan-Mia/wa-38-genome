#!/usr/bin/env python3

"""
A simple script that reads in BLAST results of the megablast results of WA38 
vs RefSeq Plastid database.  It outputs two new FASTA files
one with plastid contaning scaffolds and one without.
"""

import pandas as pd
import numpy as np
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqIO.FastaIO import FastaWriter
import argparse

def calculate_coverage(results, scaffolds):
    """
    Takes BLAST results and caculates the percent coverage
    of overlap that each scaffold has with BLAST matches.

    Parameters:
      results (Pandas.DataFrame): a Pandas Dataframe of the
          BLAST results
      scaffolds (dict): a dictonary of Bio.Seq objects where
          the keys are the scaffold names.

    Returns:
      A Pandas.Series object containing percent coverage of 
      each scaffold.
    """
    coverage = {}
    seq_names = results['query'].unique()
    for name in seq_names:
        s18 = results[results['query'] == name]
        scaffold_size = len(scaffolds[name])
        overlap = np.zeros(scaffold_size)

        def markOverlap(x):
            start = x['q.start']-1
            end = x['q.end']
            overlap[start:end] = 1
            return (overlap.sum() / scaffold_size) * 100

        coverage[name] = (s18.apply(func=markOverlap, axis=1).max())

    # Convert to a Pandas series.
    coverage = pd.Series(coverage)
    return coverage

def main():
    """
    The main function
    """
    parser = argparse.ArgumentParser(description='Filters plastid sequences using megablast results')
    parser.add_argument('--haplotype', dest='haplotype', action='store', required=True,
                        help='The haplotype results to process: "hapA" or "hapB".')

    # Parse the arguments provided by the user.
    args = parser.parse_args()
    haplotype = args.haplotype

    # Read in the sequence file in FASTA format.
    seqs = SeqIO.parse("Malus-domestica-WA_38_{}-scaffolds-v1.0.a1.fa".format(haplotype), "fasta")
    scaffolds = {}
    for seq in seqs:
        scaffolds[seq.id] = seq

    # Read in the BLAST results
    results = pd.read_csv("scffolds-{}_vs_refseq_plastid.out".format(haplotype), sep="\t", comment="#", header=None)
    results.columns=['query', 'subject', 'PID', 'alignment_length', 
                 'mismatches', 'gap opens', 'q.start', 'q.end', 's.start', 's.end', 
                 'evalue', 'bit score']

    # Calculate the coverage of plastid sequnce over the scaffold
    coverage = calculate_coverage(results, scaffolds)

    # Write out the FASTA files by separating the plastids from non-plastids
    plastid_fasta = FastaWriter('Malus-domestica-WA_38_{}-plastids-v1.0.a1.fa'.format(haplotype))
    non_plastid_fasta = FastaWriter('Malus-domestica-WA_38_{}-non_plastids-v1.0.a1.fa'.format(haplotype))
    for scaffold in scaffolds:
        if scaffold in coverage[coverage > 90].index.values:
            plastid_fasta.write_record(scaffolds[scaffold])
        else:
            non_plastid_fasta.write_record(scaffolds[scaffold])

if __name__ == "__main__":
    main()
